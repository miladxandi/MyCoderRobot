﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;

namespace MyCoderRobot
{

    class Program
    {
        static void Main(string[] args)
        {
            var t = Task.Run(() => RunBot());
            //Close console with this key, This can be great for self-Chatting with user feature.
            while (Console.ReadKey().KeyChar != '~') ;
        }
        #region
        public static async Task RunBot()
        {
            try
            {
                #region
                Console.WriteLine("Bot is initializing...");
                //You can get API
                //Console.WriteLine("Please enter your API: ");
                //string API = Console.ReadLine();
                //Or let be it as a pre - defined API token
                var Bot = new TelegramBotClient("438343756:AAH--MxqtNKE1SbgW_KurSSqkHUDnxYaFSk");
                //Bot starting to be initialize.
                var Me = await Bot.GetMeAsync();
                Console.WriteLine($"{Me.Username}");
                //initialized.
                Console.WriteLine($"{Me.Username} has been initialized.");
                Console.WriteLine("\n");
                //Clear all of additional out puts
                System.Threading.Thread.Sleep(3000);
                Console.Clear();
                Console.WriteLine("Press '~' for exit.");
                Console.WriteLine($"{Me.Username} is ready to receive message.\n");
                #endregion
                #endregion
                //offset is defined to let the robot detect which message didnt get answer.
                #region
                int offset = 0;
                Regex rgx = new Regex(@"\p{Cs}");
                //continues the loop
                while (true)
                {

                    var updates = await Bot.GetUpdatesAsync(offset, timeout: 1200);

                    foreach (var update in updates)
                    {
                        //everytime make the robot answer the newest messages, because of the incremention of ID
                        offset = update.Id + 1;
                        if (update.Message != null)
                        {
                            //ChatId used for answer.
                            var ChatId = update.Message.Chat.Id;

                            //MessageId is used to reply the answer to the request.
                            var MessageId = update.Message.MessageId;

                            //security definitions

                            var Calture = System.Globalization.CultureInfo.CurrentCulture;
                            #endregion
                            #region
                            /*if (Users.Contains(update.Message.Chat.Username))
                            {*/
                            if (update.Message.Text != null && (update.Message.Photo == null && update.Message.Video == null && update.Message.Venue == null && update.Message.Audio == null && update.Message.Document == null))
                            {
                                //make out put more interactive.
                                Console.WriteLine($"{update.Message.From.FirstName} {update.Message.From.LastName} ({update.Message.From.Username}): {update.Message.Text}");
                                var Message = update.Message.Text;
                                if (update.Message.Chat.Type != Telegram.Bot.Types.Enums.ChatType.Private)
                                {
                                    //do not let the robot to answer in the groups.
                                    continue;
                                }
                                else if (update.Message.Text.Contains("/start"))
                                {
                                    //Definition for the pre defined /start command of telegram bots.
                                    await Bot.SendTextMessageAsync(chatId: ChatId, replyToMessageId: MessageId, text: $@"Hi {update.Message.Chat.FirstName}, I {Me.Username} will be your assistance!
I can work on your C# & PHP codes.
For start coding on telegram please type 'help'.
To start coding type `Code` above your codes.
If you want to contact me, text me on [Milad](https://t.me/milad_xandi/) Telegram id.", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                                    continue;

                                }
                                #region
                                else if ((update.Message.Text.Contains("http") ||
                                    update.Message.Text.Contains("https") ||
                                    update.Message.Text.Contains(".ir") ||
                                    update.Message.Text.Contains("https") ||
                                    update.Message.Text.Contains(".com") ||
                                    update.Message.Text.Contains(".net") ||
                                    update.Message.Text.Contains("link") ||
                                    update.Message.Text.Contains("help") ||
                                    update.Message.Text.Contains("Help") ||
                                    update.Message.Text.Contains("www") ||
                                    update.Message.Text.Contains(".org")
                                    ))
                                {
                                    if (Message.Contains("[") || Message.Contains("]") || Message.Contains("(") || Message.Contains(")") || Message.Contains(":") || Message.Contains(";") || Message.Contains("*") || Message.Contains("_") || Message.Contains("`") || Message.Contains("```"))
                                    {
                                        if (Message.IndexOf("]") >= Message.IndexOf("[") && Message.IndexOf(")") >= Message.IndexOf("("))
                                        {
                                            //reply back the pre defined commands
                                            await Bot.SendTextMessageAsync(chatId: ChatId, replyToMessageId: MessageId, text: Message, parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                                            await Bot.SendTextMessageAsync(chatId: ChatId, replyToMessageId: MessageId, text: Message, parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown, disableWebPagePreview: true);
                                            continue;
                                        }
                                    }
                                    //guides
                                    await Bot.SendTextMessageAsync(chatId: ChatId, replyToMessageId: MessageId, text: @"For other developer features, please type like this:
                                **Bold Text**
                                __Italic Text__
                                [Value](http://www.example.com/)
                                [Value](tg://user?id=123456789)
                                `Single-line code`
                                ```
                                 Multi-line code
                                ```");

                                }
                                #endregion
                                else if (update.Message.Text == "Code" || update.Message.Text == "code")
                                {
                                    string firstCode = @"For example type ```Code``` or type Code above your Code to make it convert your normal text to code!";
                                    await Bot.SendTextMessageAsync(text: firstCode, parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown, chatId: ChatId, replyToMessageId: MessageId);
                                }
                                else if (update.Message.Text == "html" || update.Message.Text == "HTML" || update.Message.Text == "Html")
                                {
                                    string firstCode = $@"
```
<html>
    <head>
        <title>
            Designer:{update.Message.From.Username}
        </title>
    </head>
    <body>
        <h1>Thats {update.Message.From.FirstName} {update.Message.From.LastName}`s first HTML code</h1>
    </body>
</html>
```

@MyCoderRobot";
                                    await Bot.SendTextMessageAsync(text: firstCode, parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown, chatId: ChatId, replyToMessageId: MessageId);
                                }
                                else if (Message.Contains("Code"))
                                {
                                    
                                    string Code = Message.Remove(0, 4);
                                    

                                        await Bot.SendTextMessageAsync(chatId: ChatId, replyToMessageId: MessageId, text: $@"
```
{Code}
```", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                                    
                                    #endregion
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }

            #region
            catch (Telegram.Bot.Exceptions.ApiRequestException error)
            {
                Console.WriteLine($"{error.Message} , we`ll reset for you in 5 seconds.");
                System.Threading.Thread.Sleep(5000);
                Console.Clear();
                var t = Task.Run(() => RunBot());
            }
            catch (NullReferenceException error)
            {
                Console.WriteLine($"{error.Message} , we`ll reset for you in 5 seconds.");
                System.Threading.Thread.Sleep(5000);
                Console.Clear();
                var t = Task.Run(() => RunBot());
            }
            catch (ArgumentNullException error)
            {
                Console.WriteLine($"{error.Message} , we`ll reset for you in 5 seconds.");
                System.Threading.Thread.Sleep(5000);
                Console.Clear();
                var t = Task.Run(() => RunBot());
            }
            catch (Exception error)
            {
                Console.WriteLine($"{error.Message} , we`ll reset for you in 5 seconds.");
                System.Threading.Thread.Sleep(5000);
                Console.Clear();
                var t = Task.Run(() => RunBot());
            }
            #endregion
        }
    }
}
